export default {
  isContacts: false,
  customShowListable: 'Mostrar',
  listCustomerInfo: false,
  loggingIn: false,
  loginError: null,
  title: null,
  userInfo: null,
  currentModel: null,
  currentModule: null,
  student: null
}
