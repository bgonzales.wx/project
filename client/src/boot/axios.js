/*
* Archivo con la configuración general de axios
*/
import axios from 'axios'
import env from '../env'
import { Notify } from 'quasar'

const axiosInstance = axios.create({
  baseURL: env.apiUrl// url base cargada de archivo env.js
})

export default async ({ store, Vue }) => {
  // Vue.prototype.$axios = axios
  Vue.prototype.$api = axiosInstance

  axiosInstance.interceptors.response.use(function (response) {
    console.log('axiosResponse', response)
    // Todo bien con la respuesta
    if (response.config.method === 'post') {
      if (response.status === 200) {
        if (response.data.token === undefined) { // Si no es login
          Notify.create({
            color: 'green-4',
            icon: 'done',
            textColor: 'white',
            message: 'Registro guardado con éxito!'
          })
        } else { // Es Login
          /* let sessionInfo = {
            token: response.data.token
          } */
          localStorage.setItem('sessionInfo', JSON.stringify(response.data))
        }
      }
    }
    return response.data
  }, function (error) {
    // Error en la respuesta
    console.log('debug', error.response)
    if (error.response === undefined) { // Si no hubo comunicación con el servidor
      console.log('no hay conexion con el servidor', error)
      Notify.create({
        color: 'red-5',
        textColor: 'white',
        icon: 'fas fa-exclamation-triangle',
        message: 'No se pudo establecer conexión con el servidor'
      })
    } else { // Si el servidor dio respuesta
      console.log('linea49')
      if (error.response.status === 403) { // Error de Login
        Notify.create({
          message: error.response.data,
          color: 'red',
          position: 'center'
        })
      } else if (error.response.status === 404) { // Error de Login
        Notify.create({
          message: 'Error en ruta. Código 404',
          color: 'black',
          position: 'center'
        })
      } else if (error.response.status === 500) { // Error en servidor
        Notify.create({
          message: 'Error en servidor. Código 500',
          color: 'black',
          position: 'center'
        })
      }
      var data = error.response.data[0]
      if (data !== undefined) {
        const status = error.response.status
        if (data.field === 'email') {
          if (status === 401) {
            Notify.create({
              icon: 'warning',
              message: 'Usuario Incorrecto',
              color: 'red'
            })
          }
          if (data.statusCode === 403) {
            Notify.create({
              color: 'red-5',
              textColor: 'white',
              icon: 'fas fa-exclamation-triangle',
              message: data.message
            })
          }
          if (status === 422) {
            Notify.create({
              color: 'red-5',
              textColor: 'white',
              icon: 'warning',
              message: 'El correo ya se encentra registrado' /* + data[0].message */,
              position: 'center'
            })
            // return Promise.reject(data.response.data.error)
          }
          if (data.statusCode === 500) {
            Notify.create({
              color: 'red-5',
              textColor: 'white',
              icon: 'fas fa-exclamation-triangle',
              message: 'Error interno en servidor' + data.message
            })
            // return Promise.reject(data.response.data.error)
          }
          // Añadir mas mensajes segun codigos de error especificos y mostrar las notificaciones correspondientes
          // Notify.create(error.response.data.error.message)
          // console.log(error.response.status);
          // console.log(error.response.headers);
        } else if (data.field === 'password') {
          if (status === 401) {
            Notify.create({
              icon: 'warning',
              message: 'Contraseña Incorrecta',
              color: 'red'
            })
          }
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          // console.log(error.request)
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message)
        }
      }
      // console.log(error.config)
    }

    // return Promise.reject(data)
  })

  axiosInstance.interceptors.request.use(async function (config) {
    // Antes de enviar cada petición se añade el token si existe

    store.dispatch('generals/fetchAccessToken')
    const token = (store.state.generals.sessionInfo !== null) ? store.state.generals.sessionInfo.token : false
    console.log('token', token)
    if (token) {
      if (!config.headers) { config.headers = {} }
      config.headers = {
        Authorization: 'Bearer ' + token
      }
    }
    return config
  }, function (error) {
    // Do something with request error
    return Promise.reject(error)
  })
}

export { axiosInstance }
