
const routes = [
  {
    path: '/menu',
    component: () => import('layouts/MyMenu.vue'),
    children: [
      { path: '/menu', component: () => import('pages/Index.vue') },
      { path: '/list', component: () => import('pages/Admission/List.vue') },
      { path: '/viewdates/:dni', component: () => import('pages/Admission/Verify.vue') },
      { path: '/documents', component: () => import('pages/Students/Documents/Buy.vue') },
      { path: '/IndexStudent', component: () => import('pages/Students/IndexStudent.vue') },
      { path: '/register_counter', component: () => import('pages/Score/Table.vue') },
      { path: '/Form', component: () => import('pages/Score/Form.vue') },
      { path: '/Form/:cod', component: () => import('pages/Score/Form.vue') },
      { path: '/attach_student', component: () => import('pages/Score/Attach.vue') },
      { path: '/debts_list', component: () => import('pages/Bebts/Table.vue') },
      { path: '/information', component: () => import('pages/Students/Information/Form.vue') },
      { path: '/documents_show', component: () => import('pages/Documents/Form.vue') },
      { path: '/view_documents/:id', component: () => import('pages/Documents/View.vue') },
      { path: '/view_information/:id', component: () => import('pages/Coordinations/Information.vue') },
      { path: '/test', component: () => import('pages/Students/Test/Test.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/register',
    component: () => import('layouts/register/register.vue')
  },
  {
    path: '/login',
    component: () => import('layouts/Login.vue')
  },
  {
    path: '*',
    redirect: '/login'
  }
]

export default routes
