'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.get("/", () => {
  return { greeting: "Hello world in JSON" };
});
  
const addPrefixToGroup = group => {
 // Grupo para rutas con prefijo /api/
  group.prefix("api");
  return group;
};
addPrefixToGroup(
    Route.group(() => {
      // Insertar rutas sin protección de autenticación aquí. Rutas que si cualquiera las accede no se pone en peligro la integridad de los datos del sistema
    Route.get("public/dir/files/:filename", "DocumentsStudentController.getFile") // Ruta para arbir el archivo
    Route.post("register", "SolicitudController.create")
    Route.post("login", "UserController.login")
    })
);
addPrefixToGroup(
    Route.group(() => {
      // Rutas de la solicitud //
      Route.get('solicitudCount', 'SolicitudController.showL')
      Route.get('SolicitudUser/:dni', 'SolicitudController.solicityUser')
      Route.post('AceptedStudent/:dni', 'SolicitudController.acepted')
      Route.get('StudentAprovated', 'SolicitudController.show')
      // Rutas de la solicitud //

      // Rutas para el costo de las pruebas //
      Route.get('CounterList', 'BuyController.show')
      Route.post('AddCountTest', 'BuyController.add')
      Route.get('CounterCod/:cod', 'BuyController.showList')
      // Rutas para el costo de las pruebas //

      // Rutas para las deudas estudiantiles //
      Route.post('AddAttachStudents', 'BuyStudentController.create')
      Route.get('GetBebts', 'BuyStudentController.show')
      Route.post('AprovatedDebts/:dni', 'BuyStudentController.aprovated')
      Route.get('StudentAprovated_List', 'BuyStudentController.list')
      // Rutas para las deudas estudiantiles //

      // Rutas para los aspirantes ya aceptados y que ya pagaron //
      Route.get('GetStudent_id/:dni', 'StudentController.show')
      Route.get('GetInformation/:id', 'StudentController.shoOnly')
      Route.get('Revised_test/:dni', 'StudentController.test')
      Route.get('Revised_Documents/:dni', 'StudentController.documnetos')
      Route.post('StudentModify', 'StudentController.update')
      // Rutas para los aspirantes ya aceptados y que ya pagaron //
      
      // Rutas para acceso academico //      
      Route.get('GetStudent_Academic/:dni', 'AcademicStudentController.show')
      Route.post('StudentModify_Academic', 'AcademicStudentController.update')
      // Rutas para acceso academico //

      // Rutas de acceso Religioso //
      Route.get('GetStudent_Religion/:dni', 'ReligionStudentController.show')
      Route.post('StudentModify_Religion', 'ReligionStudentController.update')
      // Rutas de acceso Religioso //

      // Rutas de acceso Religioso Setaven //
      Route.get('GetSherpherd_Academic/:dni', 'EclesiastyStudentController.show')
      Route.post('StudentModify_Sherpherd', 'EclesiastyStudentController.update')
      // Rutas de acceso Religioso Setaven //

      // Rutas para el subido de los archivos //
      Route.post('UploadFile', 'StudentController.save')
      // Rutas para el subido de los archivos //

      // Rutas para el acceso de los documentos estudiantiles //
      Route.post('AddRutesOfDocuments', 'DocumentsStudentController.create')
      Route.get('GetStudents_Documents', 'DocumentsStudentController.show')
      Route.get('GetStudents_Documents_Filter/:title', 'DocumentsStudentController.filter')
      Route.get('GetDocuments/:id', 'DocumentsStudentController.documents')
      // Rutas para el acceso de los documentos estudiantiles //
      

    }).middleware("auth")
);
       