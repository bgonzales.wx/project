'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Religion = use('App/Models/ReligionStudent')
/**
 * Resourceful controller for interacting with religionstudents
 */
class ReligionStudentController {
  /**
   * Show a list of all religionstudents.
   * GET religionstudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new religionstudent.
   * GET religionstudents/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new religionstudent.
   * POST religionstudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single religionstudent.
   * GET religionstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let date = (await Religion.query().where({'user_id': params.dni}).fetch()).toJSON()
    response.send(date)
  }

  /**
   * Render a form to update an existing religionstudent.
   * GET religionstudents/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update religionstudent details.
   * PUT or PATCH religionstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let date = (await request.all())
    console.log(date)
    let modify = (await Religion.query().where({'user_id': date.user_id}).update({
      'religion':	date.religion,
      'type':	date.type,
      'asociate':date.asociate,
      'distitre':date.distitre,
      'sherpherd':date.sherpherd
    }))
    console.log(modify, 'Archivo MOdificado')
  }

  /**
   * Delete a religionstudent with id.
   * DELETE religionstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ReligionStudentController
