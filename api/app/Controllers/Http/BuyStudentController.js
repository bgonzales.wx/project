'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Student = use ('App/Models/Student')
const buystudent = use ('App/Models/BuyStudent')
const Religion = use('App/Models/ReligionStudent')
const User = use ('App/Models/User')
const Solicitud = use("App/Models/Solicitud")
const RoleUser = use("App/Models/RoleUser")
const Buy = use("App/Models/Buy")
const Academic = use('App/Models/AcademicStudent')
const Eclesiastic = use('App/Models/EclesiastyStudent')
const { unformat } = require('number-currency-format')
const { validate } = use("Validator")
const BuyStudentV = {
  cod_user: "required|string",
  cod_buy: "required|string",
  buy: "required|string"
 }

/**
 * Resourceful controller for interacting with buystudents
 */
class BuyStudentController {
  
  /**
   * Render a form to be used for creating a new buystudent.
   * GET buystudents/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
    let data = request.all()
    for (let i in data) {
      console.log(data)
      let save = await buystudent.create(data[i])
      response.send(save)
    }
  }

  /**
   * Create/save a new buystudent.
   * POST buystudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async aprovated ({ request, response, params}) {
    let dni = (await Solicitud.query().where({'dni': params.dni}).fetch()).toJSON()
    let aprovated = (await buystudent.query().where({'user_id': dni[0].id}).update({'aprovated': true}))
    response.send(this.createdUser(dni))
  }
  async createdUser(dni) {
    delete dni[0].aprovated
    if (dni[0].carrera === 'Teologia') {
      dni[0].carrer = 'SETAVEN'
    } else {
      dni[0].carrer = 'IUNAV'
    }
    let user = (await User.all()).toJSON()
    let infoUser = [
      {
        id: user.length + 1,
        name: 'Estudiante',
        email: dni[0].dni.slice(2,15),
        password: dni[0].name.charAt(0) + dni[0].fullname.charAt(0).toLowerCase() + '.' + dni[0].dni.slice(2,15)
      }
    ]
    let infoDetails = [
      {
        id: 0,
        user_id: 0,
        role_id: 2
      }
    ]
    for (let i in infoUser) {
      let userStudent = (await User.create(infoUser[i])).toJSON()
      infoDetails[0].id = user.length + 1
      infoDetails[0].user_id = parseInt(userStudent.id)
      console.log(infoDetails, 'details')
      let role_permission = (await RoleUser.create(infoDetails[0])).toJSON()
      let studentCreate = (await Student.create(dni[0]))
      this.createMoreDates(studentCreate)
      return studentCreate
    }
  }
  async createMoreDates(dates) {
    let infoAcademic = [{
      user_id: parseInt(dates.id),
      name: '1',
      dependency: 'Oficial',
      especiality: 'Bachiller en Ciencias',
      year: '2010/07/13',
      ubicate: 'Barquisimeto/Lara/Venezuela',
      system: 'Regular'
    }]
    await Academic.create(infoAcademic[0])

    if (dates.carrer === 'IUNAV') {
      let infoReligion = [{
        user_id: parseInt(dates.id),
        religion: true,
        asociate: '1',
        distitre: '1',
        sherpherd: '1'
      }]
      await Religion.create(infoReligion[0])
    } else if (dates.carrer === 'SETAVEN') {      
      let infoReligion = [{
        user_id: parseInt(dates.id),
        asociation: 1,
        distitre: 1,
        sherpherd: 1,
        test_sherpherd: 1,
        colport: 1,
        years_bautizate: 1,
        adventist_naciment: 1,
        bautizate: '2020/07/13',
        state_bautizate: 1,
        sherpherd_bautizate: 1
      }]
      await Eclesiastic.create(infoReligion[0])
    }
    console.log('Finish')
  }

  /**
   * Display a single buystudent.
   * GET buystudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let dates = (await buystudent.all()).toJSON()
    let Student = []
    for (let i in dates) {
      if (dates[i].aprovated === false) {
        const user = (await Solicitud.find(dates[i].user_id)).toJSON()
        const buy = (await Buy.find(dates[i].buy_id)).toJSON()
        Student.push({'name': user.name,'carrera': user.carrera, 'dni': user.dni, 'concept': buy.concept, 'count': unformat(buy.count)})
      }
    }
    /* var end = function (myarray, prop) {
     return myarray.reduce(function(groups, item) {
       var val = item[prop]
       groups[val] = groups[val] || {name: item.name, carrera: item.carrera, dni: item.dni, concept: '', count: 0}
       groups[val].count += item.count 
       groups[val].concept += item.concept+ ' '
       return groups
     }, {})
   } */ //Funcion para gacer pagos multiples, que el usuario se le carguen todas las deudas en una sola
   // let list = Object.values(end(Student, 'name'))
   // Agregado de los botones de cada uno
   let list = Student
   for (let i in list) {
    var element = list[i]
    element.actions = [
      {
        color: "negative",
        icon: "done",
        url: element.dni,
        action: "",
        title: "Detallar deuda"
      }
    ]
   }
   console.log(list)
   response.send(Student)
  }

  /**
   * Render a form to update an existing buystudent.
   * GET buystudents/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async list ({ params, request, response, view }) {
    let student = (await Solicitud.all()).toJSON()
    let buyStudents = []
    for (let i in student) {
      if (student[i].aprovated === true) {
        buyStudents.push({'name': student[i].name, 'carrera': student[i].carrera, 'dni':student[i].dni, 'id':student[i].id})
      }
    }
    response.send(buyStudents)
  }

  /**
   * Update buystudent details.
   * PUT or PATCH buystudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a buystudent with id.
   * DELETE buystudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = BuyStudentController
