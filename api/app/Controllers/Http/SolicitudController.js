'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Solicitud = use("App/Models/Solicitud")
const User = use("App/Models/User")
const Mail = use('Mail')
const RoleUser = use("App/Models/RoleUser")
const { validate } = use("Validator")
const solicitudV = {
  fullname: "required|string",
  name: "required|string",
  dni: "required|string",
  pais: "required|string",
  sexo: "required|string",
  email: "required|string",
  carrera: "required|string",
  aprovated: "required|boolean",
  date: "required|string"
 }
/**
 * Resourceful controller for interacting with solicituds
 */
class SolicitudController {
  /**
   * Show a list of all solicituds.
   * GET solicituds
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async showL ({ request, response, view }) {
    console.log(Solicitud.all())
    let dates = (await Solicitud.query().where({'aprovated': false}).fetch()).toJSON()
    let info = []
    for (let i in dates) {
      var element = dates[i]
      element.actions = [
        {
          color: "primary",
          icon: "edit",
          url: "/viewdates/" + element.dni,
          action: "",
          title: "Observar"
        }
      ]
      info.push(element)
    }
    response.send(info)
  }

  /**
   * Render a form to be used for creating a new solicitud.
   * GET solicituds/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
    const validation = await validate(request.all(), solicitudV)
    if (!validation.fails()) {
      let id = (await Solicitud.all()).toJSON()
      let body = request.only(['fullname', 'name', 'dni', 'pais', 'sexo', 'date', 'carrera', 'aprovated', 'email'])
      body.id = id.length + 1
      const solicitudes = await Solicitud.create(body)
      response.send(solicitudes)
    } else {
      response.badRequest(validation.messages())
    }
  }

  /**
   * Create/save a new solicitud.
   * POST solicituds
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async acepted ({ params, response }) {
    let aprovatedUser = (await Solicitud.query().where({'dni': params.dni}).update({'aprovated': true}))
    let Maile = await Mail.raw('Prueba de texto plano', (message) => {
      message.from('pasantias@iunav.com', 'Sistema de Admision')
      message.to('bairongonzalez15@gmail.com')
    })
    response.send(Maile)
  }
  

  /**
   * Create/save a new solicitud.
   * POST solicituds
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async createUser ({ params, response }) {
    let dates = (await Solicitud.query().where({'dni': params.dni}).fetch()).toJSON()
    let dni = dates[0].dni
    let infoUser = [
      {
        name: 'Estudiante',
        email: dni.slice(2, 15),
        password: dates[0].name.charAt(0) + dates[0].fullname.charAt(0).toLowerCase() + "." + dni.slice(2, 15)
      }
    ]
    for (let i in infoUser) {
      let exist = (await User.query().where({'email': infoUser[i].email}).fetch()).toJSON()
      if (exist.length === 0) {
        const user = await User.create(infoUser[i])
        let infoPermission = [{
          user_id: user.id,
          role_id: 2
        }]
        const role_permission = await RoleUser.create(infoPermission[0])
        console.log(role_permission, 'Creacion del rol')
        dates[0].aprovated = true
        let aprovatedUser = (await Solicitud.query().where({'dni': params.dni}).update({'aprovated': true}))
        response.send(user)
      } else {
        response.send('Usuario ya existente')
      }
      
    }
  }

  /**
   * Display a single solicitud.
   * GET solicituds/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async solicityUser ({ params, request, response, view }) {
    let dates = (await Solicitud.query().where({'dni': params.dni}).fetch()).toJSON()
    response.send(dates)
  }

  /**
   * Render a form to update an existing solicitud.
   * GET solicituds/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {    
    let dates = (await Solicitud.query().where({'aprovated': true}).fetch()).toJSON()
    response.send(dates)
  }

  /**
   * Update solicitud details.
   * PUT or PATCH solicituds/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a solicitud with id.
   * DELETE solicituds/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = SolicitudController
