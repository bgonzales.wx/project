'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Academic = use('App/Models/AcademicStudent')

/**
 * Resourceful controller for interacting with academicstudents
 */
class AcademicStudentController {
  /**
   * Show a list of all academicstudents.
   * GET academicstudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new academicstudent.
   * GET academicstudents/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new academicstudent.
   * POST academicstudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single academicstudent.
   * GET academicstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let data = (await Academic.query().where({'user_id': params.dni}).fetch()).toJSON()
    response.send(data)
  }

  /**
   * Render a form to update an existing academicstudent.
   * GET academicstudents/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update academicstudent details.
   * PUT or PATCH academicstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let date = (await request.all())
    let modify = (await Academic.query().where({'user_id': date.user_id}).update({
      'name': date.name,
      'dependency':date.dependency,
      'especiality':date.especiality,
      'year':date.year,
      'ubicate':date.ubicate,
      'system':date.system,
      'superiority_carrera':date.superiority_carrera,
      'superiority_insitute':date.superiority_insitute
    }))
    console.log(modify, 'Archivo MOdificado')
  }

  /**
   * Delete a academicstudent with id.
   * DELETE academicstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = AcademicStudentController
