'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Buy = use("App/Models/Buy")
const { validate } = use("Validator")
const CountV = {
  cod: "required|string",
  concept: "required|string",
  count: "required|string"
 }

/**
 * Resourceful controller for interacting with buys
 */
class BuyController {
  /**
   * Show a list of all buys.
   * GET buys
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new buy.
   * GET buys/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new buy.
   * POST buys
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single buy.
   * GET buys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let dates = (await Buy.all()).toJSON()
    let info = []
    for (let i in dates) {
      var element = dates[i]
      element.actions = [
        {
          color: "primary",
          icon: "edit",
          url: "/Form/" + element.cod,
          action: "",
          title: "Observar"
        }
      ]
      info.push(element)
    }
    response.send(info)
  }

  /**
   * Render a form to update an existing buy.
   * GET buys/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async add ({ params, request, response, view }) {
    const validation = await validate(request.all(), CountV)
    if (!validation.fails()) {
      const body = request.only(['cod', 'concept', 'count'])
      let date = (await Buy.query().where({'cod': body.cod}).fetch()).toJSON()
      if(date.length === 0) {
        let id = (await Buy.all()).toJSON()
        body.id = id.length + 1
        await Buy.create(body)
        response.send('Created')
      } else {
        let changes = (await Buy.query().where({'cod': body.cod}).update({
          'count': body.count,
          'concept': body.concept
        }))
        response.send('Edit')
      }
    } else {
      response.badRequest(validation.messages())
    }
  }

  /**
   * Update buy details.
   * PUT or PATCH buys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async showList ({ params, request, response }) {
    let dates = (await Buy.query().where({'cod': params.cod}).fetch()).toJSON()
    response.send(dates)
  }

  /**
   * Delete a buy with id.
   * DELETE buys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = BuyController
