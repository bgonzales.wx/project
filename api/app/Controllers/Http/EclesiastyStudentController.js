'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Eclesiastic = use('App/Models/EclesiastyStudent')

/**
 * Resourceful controller for interacting with eclesiastystudents
 */
class EclesiastyStudentController {
  /**
   * Show a list of all eclesiastystudents.
   * GET eclesiastystudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new eclesiastystudent.
   * GET eclesiastystudents/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new eclesiastystudent.
   * POST eclesiastystudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single eclesiastystudent.
   * GET eclesiastystudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let date = (await Eclesiastic.query().where({'user_id': params.dni}).fetch()).toJSON()
    response.send(date)
  }

  /**
   * Render a form to update an existing eclesiastystudent.
   * GET eclesiastystudents/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update eclesiastystudent details.
   * PUT or PATCH eclesiastystudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let date = (await request.all())
    let modify = (await Eclesiastic.query().where({'user_id': date.user_id}).update({
      'asociation': date.asociation,
      'distitre': date.distitre,
      'sherpherd': date.sherpherd,
      'test_sherpherd': date.test_sherpherd,
      'colport': date.colport,
      'years_bautizate': date.years_bautizate,
      'adventist_naciment': date.adventist_naciment,
      'bautizate': date.bautizate,
      'state_bautizate': date.state_bautizate,
      'sherpherd_bautizate':date.sherpherd_bautizate
    }))
    console.log('Sherpherd Modify', modify)
  }

  /**
   * Delete a eclesiastystudent with id.
   * DELETE eclesiastystudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EclesiastyStudentController
