'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Student = use('App/Models/Student')
const Academic = use('App/Models/AcademicStudent')
const Religion = use('App/Models/ReligionStudent')
const Eclesiastic = use('App/Models/EclesiastyStudent')
const Document = use('App/Models/DocumentsStudent')
/**
 * Resourceful controller for interacting with students
 */
class StudentController {
  /**
   * Show a list of all students.
   * GET students
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new student.
   * GET students/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Display a single student.
   * GET students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async save ({ request, response, auth }) {
    // Busco el estudiante logueado y a este le busco el id para añadirlo a los archivos para un mejor orden
    let user = await auth.getUser()
    let student = (await Student.query().where({'dni': 'V-'+user.email}).fetch()).toJSON()
    if (!student) {
      student = (await Student.query().where({'dni': 'E-'+user.email}).fetch()).toJSON()
    }
    const documents = request.file('file', {
      types: ['pdf'],
      size: '4mb'
    })
    const dir = 'public/dir/files'
    let fileName = documents.clientName.replace(/\s/g, '')
    fileName = student[0].id + '_' +fileName.split('-').join('')
    if (documents) {
      await documents.move(dir, {
        name: fileName,
        overwrite: true
      })
      if (!documents.moved()) {
        return response.status(422).send({
          status: true,
          message: 'Ocurrio un error al cargar los archivos',
          icon: 'warning',
          color: 'negative'
        })
      }
      return response.status(201).send({
        status: true,
        message: 'Archivios cargados exitosamente',
        icon: 'done',
        color: 'positive',
        info: [{
          'user_id': student[0].id,
          'dir': `${dir}/${fileName}`
        }]
      })
    } else {
      return response.status(422).send({
        status: true,
        message: 'Ocurrio un error al cargar los archivos',
        icon: 'warning',
        color: 'negative'
      })
    }
  }

  /**
   * Display a single student.
   * GET students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let data = (await Student.query().where({'dni': params.dni}).fetch()).toJSON()
    response.send(data)
  }
  async shoOnly ({ params, request, response, view }) {
    let dateGeneral = []
    let data = (await Student.query().where({'id': params.id}).fetch()).toJSON()
    let academic = (await Academic.query().where({'user_id': params.id}).fetch()).toJSON()
    let religion = (await Religion.query().where({'user_id': params.id}).fetch()).toJSON()
    let eclesiastic = (await Eclesiastic.query().where({'user_id': params.id}).fetch()).toJSON()
    dateGeneral.push({'personal': data, 'academic': academic, 'religion': religion, 'eclesiastic': eclesiastic})
    console.log(dateGeneral)
    response.send(dateGeneral)
  }

  /**
   * Render a form to update an existing student.
   * GET students/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async test ({ params, request, response, view }) {
    let dates = (await Student.query().where({'dni': params.dni}).fetch()).toJSON()
    let band = false
    console.log(dates, 'INformation')
    for (let i in dates) {
      if (dates[i].phone === null) {
        band = true
        console.log('Faltan Datos personales')
      } else {
        let academic = (await Academic.query().where({'user_id': parseInt(dates[0].id)}).fetch()).toJSON()
        console.log(academic)
        if (academic[0].name === '1') {
          band = true
          console.log('Faltan Datos Academicos')
        } else {
          if (dates[0].carrer === 'IUNAV') {
            let religion = (await Religion.query().where({'user_id': parseInt(dates[0].id)}).fetch()).toJSON()
            if (religion[0].type === '1') {
              band = true
              console.log('Faltan Datos Religiosos')
            } else {
              band = false
              console.log('Esta listo todo, siguiente paso')
            }
          } else if (dates[0].carrer === 'SETAVEN') {
            let eclesiasticStudent = (await Eclesiastic.query().where({'user_id': parseInt(dates[0].id)}).fetch()).toJSON()
            if (eclesiasticStudent[0].asociation === '1') {
              band = true
              console.log('Faltan Datos Religiosos')
            } else {
              band = false
              console.log('Esta listo todo, siguiente paso')
            }
          }
        }
      }
    }
    response.send(band)
  }

  /**
   * Update student details.
   * PUT or PATCH students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async documnetos ({params, request, response}) {
    let dates = (await Student.query().where({'dni': params.dni}).fetch()).toJSON()
    let document = (await Document.query().where({'user_id': dates[0].id}).fetch()).toJSON()
    if (document.length === 0) {
      response.send(true)
    } else {
      response.send(false)
    }
  }

  /**
   * Update student details.
   * PUT or PATCH students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let date = (await request.all())
    let modify = (await Student.query().where({'dni': date.dni}).update({
      'name': date.name,
      'fullname': date.fullname,
      'dni': date.dni,
      'date': date.date,
      'year': date.year,
      'sexo': date.sexo,
      'civil_state': date.civil_state,
      'pais': date.pais,
      'carrera': date.carrera,
      'carrer': date.carrer,
      'email': date.email,
      'phone': date.phone,
      'local_phone': date.local_phone,
      'father_fullname': date.father_fullname,
      'mother_fullname': date.mother_fullname,
      'father_phone': date.father_phone,
      'mother_phone': date.mother_phone,
      'direction': date.direction,
      'direction_pais': date.direction_pais,
      'direction_state': date.direction_state,
      'direction_city': date.direction_city
    }))
    console.log(modify, 'Archivo MOdificado')
  }

  /**
   * Delete a student with id.
   * DELETE students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = StudentController
