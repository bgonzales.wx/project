'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Document = use('App/Models/DocumentsStudent')
const Helpers = use('Helpers')
const Student = use('App/Models/Student')
const { validate } = use("Validator")
const CountV = {
  file: "required|string",
  user_id: "required|string",
  dir: "required|string"
 }
/**
 * Resourceful controller for interacting with documentsstudents
 */
class DocumentsStudentController {
  /**
   * Show a list of all documentsstudents.
   * GET documentsstudents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async verufy ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new documentsstudent.
   * GET documentsstudents/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
    const validation = await validate(request.all(), CountV)
    if (!validation.fails()) {
      const body = request.only(['dir', 'user_id', 'file'])
      console.log(body)
      let eyes = (await Document.query().where({'user_id': body.user_id, 'file': body.file}).fetch()).toJSON()
      if (eyes.length === 0) {
        let create = (await Document.create(body))
        response.status(201).send(create)
        console.log('Created')
      } else {
        let changes = (await Document.query().where({'user_id': body.user_id, 'file': body.file}).update({
          'dir': body.dir
        }))
        response.status(204).send(changes)
        console.log('Update')
      }
    }
  }

  /**
   * Display a single documentsstudent.
   * GET documentsstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async filter ({ params, request, response, view }) {
    let documents = (await Document.all()).toJSON()
    let student = []
    let info = []
    for (let i in documents) {
      if (params.title === 'gerent') {
        student = (await Student.query().where({'id': parseInt(documents[i].user_id), 'carrera': 'Empresas'}).fetch()).toJSON()
        console.log(student)
        if (student.length === 0) {
          student = (await Student.query().where({'id': parseInt(documents[i].user_id), 'carrera': 'Personal'}).fetch()).toJSON()
          console.log(student)
        } else {
          info.push({ 'name': student[0].name, 'carrera': student[0].carrera, 'actions': [
            {
              color: "secondary",
              icon: "visibility",
              url: `view_information/${student[0].id}`,
              action: "",
              title: "Observar Informacion"
            }
          ] })     
        }
      } else if (params.title === 'informatic') {
        student = (await Student.query().where({'id': parseInt(documents[i].user_id), 'carrera': 'Informatica'}).fetch()).toJSON()
        console.log(student)
        if (student.length === 0) {
        console.log('Vacio')
      } else {
        info.push({ 'name': student[0].name, 'carrera': student[0].carrera, 'actions': [
          {
            color: "secondary",
            icon: "visibility",
            url: `view_information/${student[0].id}`,
            action: "",
            title: "Observar Informacion"
          }
        ] })     
      }
      } else if (params.title === 'education') {
        student = (await Student.query().where({'id': parseInt(documents[i].user_id), 'carrera': 'Prescolar'}).fetch()).toJSON()
        console.log(student)
        if (student.length === 0) {
          console.log('Vacio')
        } else {
          info.push({ 'name': student[0].name, 'carrera': student[0].carrera, 'actions': [
            {
              color: "secondary",
              icon: "visibility",
              url: `view_information/${student[0].id}`,
              action: "",
              title: "Observar Informacion"
            }
          ] })     
        }
      }  else if (params.title === 'teology') {
        student = (await Student.query().where({'id': parseInt(documents[i].user_id), 'carrera': 'Teologia'}).fetch()).toJSON()
        console.log(student)
        if (student.length === 0) {
          console.log('Vacio')
        } else {
          info.push({ 'name': student[0].name, 'carrera': student[0].carrera, 'actions': [
            {
              color: "secondary",
              icon: "visibility",
              url: `view_information/${student[0].id}`,
              action: "",
              title: "Observar Informacion"
            }
          ] })     
        }
      }
    }
    var end = function (myarray, prop) {
      return myarray.reduce(function(groups, item) {
        var val = item[prop]
        groups[val] = groups[val] || {name: item.name, carrera: item.carrera, actions: item.actions }
        return groups
      }, {})
    }
    let list = Object.values(end(info, 'name'))
    response.send(list)
  }

  /**
   * Display a single documentsstudent.
   * GET documentsstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let date = (await Document.all()).toJSON()
    let info = []
    for (let i in date) {
      let student = (await Student.query().where({'id': parseInt(date[i].user_id)}).fetch()).toJSON()
      info.push({ 'name': student[0].name, 'carrera': student[0].carrera, 'actions': [
        {
          color: "secondary",
          icon: "visibility",
          url: `view_documents/${student[0].id}`,
          action: "",
          title: "Observar documentos"
        }
      ] })
    }
    var end = function (myarray, prop) {
     return myarray.reduce(function(groups, item) {
       var val = item[prop]
       groups[val] = groups[val] || {name: item.name, carrera: item.carrera, actions: item.actions }
       return groups
     }, {})
   }
   let list = Object.values(end(info, 'name'))
    response.send(list)
  }

  async getFile ({ params, response }) {
    const fileName = params.filename
    console.log(fileName)
    response.download(Helpers.appRoot('/public/dir/files/') + `${fileName}`)
  }

  /**
   * Render a form to update an existing documentsstudent.
   * GET documentsstudents/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async documents ({ params, request, response, view }) {
    let documents = (await Document.query().where({'user_id': params.id}).fetch()).toJSON()
    let info = []
    for (let i in documents) {
      info.push(
        {
          color: "secondary",
          icon: "folder",
          url: `${documents[i].dir}`,
          title: `${documents[i].file}`
        }
      )
    }
    console.log(info)
    response.send(info)
  }

  /**
   * Update documentsstudent details.
   * PUT or PATCH documentsstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a documentsstudent with id.
   * DELETE documentsstudents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = DocumentsStudentController
