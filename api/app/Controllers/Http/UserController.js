'use strict'
const User = use('App/Models/User')
const Rol = use('App/Models/Role')
const Stundents = use("App/Models/Student")
const RoleUser = use('App/Models/RoleUser')
const RolePermission = use('App/Models/RolePermission')
const Permission = use('App/Models/Permission')
class UserController {
    /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  
  async login ({ auth, request }) {
    let student
    const { email, password } = request.all();
    let token = await auth.attempt(email, password)
    let user = (await User.query().where({'email' : email}).fetch()).toJSON()
    let Role = (await RoleUser.query().where({'user_id' : user[0].id}).fetch()).toJSON()
    let permission = (await RolePermission.query().where({'role_id' : Role[0].role_id}).fetch()).toJSON()
    let allPermissions = []
    for (let i in permission) {
      let permi = (await Permission.query().where({'id': permission[i].permission_id}).fetch()).toJSON()
      allPermissions.push(permi[0].slug)
    }
    if (user[0].name === 'Estudiante') {
      student = (await Stundents.query().where({'dni': 'V-'+user[0].email}).fetch()).toJSON()
      if (!student) {
        student = (await Stundents.query().where({'dni': 'E-'+user[0].email}).fetch()).toJSON()
      }
      token.Student = student[0].dni // Agrego el Estudiante Logueado para la busqueda de los datos de este mismo
    } else {
      const name = (await Rol.find(Role[0].role_id))
      token.Student = name.slug
    }
    token.permissions = allPermissions
    token.title = user[0].name //Titulo para el menu
    return token
  }
}

module.exports = UserController
