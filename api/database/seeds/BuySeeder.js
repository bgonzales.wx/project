'use strict'

/*
|--------------------------------------------------------------------------
| BuySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Buy = use('App/Models/Buy')
const BuyInfo = [
  {
    id: 1,
    cod: '001',
    concept: 'Prueba IUNAV',
    count: '2.300.00,00'
  },
  {
    id: 2,
    cod: '002',
    concept: 'Prueba SETAVEN',
    count: '4.300.00,00'
  }
]
class BuySeeder {
  async run () {
    for (let i in BuyInfo) {
      let buys = await Buy.find(BuyInfo[i].id)
      if (!buys) {
        await Buy.create(BuyInfo[i])
      } else {
        buys.cod = BuyInfo[i].cod
        buys.concept = BuyInfo[i].concept
        buys.count = BuyInfo[i].count
        await buys.save()
      }
      console.log('Buys Create')
    }
  }
}

module.exports = BuySeeder
