'use strict'

/*
|--------------------------------------------------------------------------
| PermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Permission = use ("App/Models/Permission")
const PermissionData = [
  {
    id: 1,
    name: 'Alumnos',
    slug: 'alumns',
    description: 'Alumnos pendientes'
  },
  {
    id: 2,
    name: 'Listado de Alumnos Solicitantes',
    slug: 'alumns.list',
    description: 'Listado de solicitudes'
  },
  {
    id: 3,
    name: 'Pruebas',
    slug: 'admission',
    description: 'Proceso de admision'
  },
  {
    id: 4,
    name: 'Documentos',
    slug: 'admission.documents',
    description: 'Cargado de Pagos'
  },
  {
    id: 5,
    name: 'Presentacion de Pruebas',
    slug: 'admission.test',
    description: 'Pruebas Psicotecnicas'
  },
  {
    id: 6,
    name: 'Precion de Pruebas',
    slug: 'counter',
    description: 'Controlador del precio de las Pruebas Psicotecnicas'
  },
  {
    id: 7,
    name: 'Agregar Pagos',
    slug: 'attach_payment',
    description: 'Adjuntar pagos a Estudiantes'
  },
  {
    id: 8,
    name: 'Listado de Deudores',
    slug: 'debts.list',
    description: 'Listado de cada estudiante que posee una deuda'
  },
  {
    id: 9,
    name: 'Documentos',
    slug: 'documents',
    description: 'Modulo de documentos estudiantiles'
  },
  {
    id: 10,
    name: 'Listado de Documentos',
    slug: 'documents.show',
    description: 'Listado de documentos'
  },
  {
    id: 11,
    name: 'Solicitantes',
    slug: 'documents.show_cordination',
    description: 'Listado de documentos'
  },
  {
    id: 12,
    name: 'Pruebas',
    slug: 'test_order',
    description: 'Pruebas Presentadas por Alumno'
  },
  {
    id: 13,
    name: 'Documentos',
    slug: 'document.se',
    description: 'Listado de documentos'
  }
]
class PermissionSeeder {
  async run () {
    for (let i in PermissionData) {
      let permission = await Permission.find(PermissionData[i].id)
      if (!permission) {
        await Permission.create(PermissionData[i])
      } else {
        permission.name = PermissionData[i].name,
        permission.slug = PermissionData[i].slug,
        permission.description = PermissionData[i].description
        await permission.save()
      }
      console.log('Permission Finish')
    }
  }
}

module.exports = PermissionSeeder
