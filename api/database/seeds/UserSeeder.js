'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const RoleUser = use('App/Models/User')
const Role = use('App/Models/RoleUser')
const RoleUserId = [
  {
    id: 1,
    user_id: 1,
    role_id: 1
  },
  {
    id: 2,
    user_id: 2,
    role_id: 3
  },
  {
    id: 3,
    user_id: 3,
    role_id: 4
  },
  {
    id: 4,
    user_id: 4,
    role_id: 5
  },
  {
    id: 5,
    user_id: 5,
    role_id: 6
  },
  {
    id: 6,
    user_id: 6,
    role_id: 7
  },
  {
    id: 7,
    user_id: 7,
    role_id: 8
  },
  {
    id: 8,
    user_id: 8,
    role_id: 9
  },
  {
    id: 9,
    user_id: 9,
    role_id: 10
  }
]
const RoleUserData = [
  {
    id: 1,
    name: 'Administración',
    email: 'administracion@iunav.com',
    password: 'admin'
  },
  {
    id: 2,
    name: 'Informatica',
    email: 'informatica@iunav.com',
    password: 'admin'
  },
  {
    id: 3,
    name: 'Administracion',
    email: 'admon@iunav.com',
    password: 'admin'
  },
  {
    id: 4,
    name: 'Educacion',
    email: 'educacion@iunav.com',
    password: 'admin'
  },
  {
    id: 5,
    name: 'Teologia',
    email: 'teologia@iunav.com',
    password: 'admin'
  },
  {
    id: 6,
    name: 'Servicios Estudiantiles',
    email: 'se@iunav.com',
    password: 'admin'
  },
  {
    id: 7,
    name: 'Dpto Orientacion',
    email: 'orientacion@iunav.com',
    password: 'admin'
  },
  {
    id: 8,
    name: 'Finanzas',
    email: 'finanzas@iunav.com',
    password: 'admin'
  },
  {
    id: 9,
    name: 'Control de Estudios',
    email: 'ce@iunav.com',
    password: 'admin'
  }
]
class UserSeeder {
  async run () {
    for (let i in RoleUserData) {
      let dates = await RoleUser.find(RoleUserData[i].id)
      if(!dates) {
        await RoleUser.create(RoleUserData[i])
      } else {
        dates.name = RoleUserData[i].name,
        dates.email = RoleUserData[i].email,
        dates.password = RoleUserData[i].password
        dates.save()
      }
      console.log('User Finish')
    }    
    for (let i in RoleUserId) {
      let dates = await Role.find(RoleUserId[i].id)
      if (!dates) {
        await Role.create(RoleUserId[i])
      } else {
        dates.user_id = RoleUserId[i].user_id,
        dates.role_id = RoleUserId[i].role_id
        dates.save()
      }
      console.log('Role_User Finish')
    }
  }
}

module.exports = UserSeeder
