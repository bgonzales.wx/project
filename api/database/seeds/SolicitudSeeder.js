'use strict'

/*
|--------------------------------------------------------------------------
| SolicitudSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Solicitud = use('App/Models/Solicitud')
const SolicitudInfo = [
  {
    id: 1,
    name: 'Jonathan Cristopher',
    fullname: 'GOnzalez Zurita',
    dni: 'V-26532622',
    date: '1998-09-25',
    sexo: 'M',
    pais: 'VEN',
    carrera: 'Informatica',
    email: 'bairongonzalez15@gmail.com',
    aprovated: false
  }
]
class SolicitudSeeder {
  async run () {
    for (let i in SolicitudInfo) {
      let verify = await Solicitud.find(SolicitudInfo[i].id)
      if (!verify) {
        await Solicitud.create(SolicitudInfo[i])
      } else {
        verify.name = SolicitudInfo[i].name
        verify.fullname = SolicitudInfo[i].fullname
        verify.dni = SolicitudInfo[i].dni
        verify.date = SolicitudInfo[i].date
        verify.sexo = SolicitudInfo[i].sexo
        verify.pais = SolicitudInfo[i].pais
        verify.carrera = SolicitudInfo[i].carrera
        verify.email = SolicitudInfo[i].email
        verify.aprovated = SolicitudInfo[i].aprovated
        await verify.save()
      }
      console.log('Solicituds created')
    }
  }
}

module.exports = SolicitudSeeder
