'use strict'

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Role = use("App/Models/Role")
const RolePermission = use ("App/Models/RolePermission")
const RolePermissionData = [
  {
    id: 1,
    permission_id: 1,
    role_id: 1
  },
  {
    id: 2,
    permission_id: 2,
    role_id: 1
  },
  {
    id: 3,
    permission_id: 3,
    role_id: 2
  },
  {
    id: 4,
    permission_id: 4,
    role_id: 2
  },
  {
    id: 5,
    permission_id: 5,
    role_id: 2
  },
  {
    id: 6,
    permission_id: 6,
    role_id: 9
  },
  {
    id: 7,
    permission_id: 7,
    role_id: 9
  },
  {
    id: 8,
    permission_id: 8,
    role_id: 9
  },
  {
    id: 9,
    permission_id: 3,
    role_id: 10
  },
  {
    id: 10,
    permission_id: 4,
    role_id: 10
  },
  {
    id: 11,
    permission_id: 11,
    role_id: 3
  },
  {
    id: 12,
    permission_id: 11,
    role_id: 4
  },
  {
    id: 13,
    permission_id: 11,
    role_id: 5
  },
  {
    id: 14,
    permission_id: 11,
    role_id: 6
  },
  {
    id: 15,
    permission_id: 13,
    role_id: 7
  },
  {
    id: 16,
    permission_id: 12,
    role_id: 8
  }
]
const RoleData = [
  {
    id: 1,
    name: 'Administración',
    slug: 'admin',
    description: 'Rol General de Administrador'
  },
  {
    id: 2,
    name: 'Estudiante',
    slug: 'student',
    description: 'Rol General del Estudiante'
  },
  {
    id: 3,
    name: 'Informatica',
    slug: 'informatic',
    description: 'Rol General de la Coordinación'
  },
  {
    id: 4,
    name: 'Administracion',
    slug: 'gerent',
    description: 'Rol General de la Coordinación'
  },
  {
    id: 5,
    name: 'Educación P.',
    slug: 'education',
    description: 'Rol General de la Coordinación'
  },
  {
    id: 6,
    name: 'Teologia',
    slug: 'teology',
    description: 'Rol General de la Coordinación'
  },
  {
    id: 7,
    name: 'Servicios Estudiantiles',
    slug: 'studiantil_service',
    description: 'Rol General de Servicios Estudiantiles'
  },
  {
    id: 8,
    name: 'Dpto. Orientacion',
    slug: 'order',
    description: 'Rol General del Departamento de Orientacion'
  },
  {
    id: 9,
    name: 'Finanzas Estudiantiles',
    slug: 'finanzas',
    description: 'Rol General de Finanzas'
  },
  {
    id: 10,
    name: 'Control de Estudios',
    slug: 'control',
    description: 'Rol General de Control de Estudios'
  }
]
class RoleSeeder {
  async run () {
    for (let i in RoleData) {
      let role = await Role.find(RoleData[i].id)
      if (!role) {
        await Role.create(RoleData[i])
      } else {
        role.name = RoleData[i].name,
        role.slug = RoleData[i].slug,
        role.description = RoleData[i].description
        await role.save()
      }
      console.log('Role Finish')
    }
    
    for (let i in RolePermissionData) {
      let r_p = await RolePermission.find(RolePermissionData[i].id)
      if(!r_p) {
        await RolePermission.create(RolePermissionData[i])
      } else {
        r_p.permission_id = RolePermissionData[i].permission_id,
        r_p.role_id = RolePermissionData[i].role_id
        await r_p.save()
      }
      console.log('Role and Permission Finish')
    }
  }
}

module.exports = RoleSeeder
