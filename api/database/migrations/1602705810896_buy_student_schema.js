'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BuyStudentSchema extends Schema {
  up () {
    this.create('buy_students', (table) => {
      table.bigIncrements('id')
      table.bigInteger('user_id').index()
      table.foreign('user_id').references('id').on('solicituds').onDelete('cascade')
      table.bigInteger('buy_id').index()
      table.foreign('buy_id').references('id').on('buys').onDelete('cascade')
      table.boolean('aprovated', 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('buy_students')
  }
}

module.exports = BuyStudentSchema
