'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BuySchema extends Schema {
  up () {
    this.create('buys', (table) => {
      table.bigIncrements('id')
      table.string('cod', 200).notNullable()
      table.string('concept', 200).notNullable()
      table.string('count', 50).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('buys')
  }
}

module.exports = BuySchema
