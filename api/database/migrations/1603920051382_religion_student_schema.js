'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReligionStudentSchema extends Schema {
  up () {
    this.create('religion_students', (table) => {
      table.bigIncrements('id')
      table.bigInteger('user_id').index()
      table.foreign('user_id').references('id').on('students').onDelete('cascade')
      table.boolean('religion').notNullable()
      table.string('type', 80)
      table.string('asociate', 100)
      table.string('distitre', 100)
      table.string('sherpherd', 100)
      table.timestamps()
    })
  }

  down () {
    this.drop('religion_students')
  }
}

module.exports = ReligionStudentSchema
