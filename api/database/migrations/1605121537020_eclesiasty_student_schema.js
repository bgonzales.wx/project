'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EclesiastyStudentSchema extends Schema {
  up () {
    this.create('eclesiasty_students', (table) => {
      table.bigIncrements('id')
      table.bigInteger('user_id').index()
      table.foreign('user_id').references('id').on('students').onDelete('cascade')
      table.string('asociation', 200).notNullable()
      table.string('distitre', 200).notNullable()
      table.string('sherpherd', 200).notNullable()
      table.boolean('test_sherpherd', 2).notNullable()
      table.boolean('colport', 2).notNullable()
      table.string('years_bautizate', 4).notNullable()
      table.boolean('adventist_naciment', 2).notNullable()
      table.date('bautizate').notNullable()
      table.string('state_bautizate', 200).notNullable()
      table.string('sherpherd_bautizate', 200).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('eclesiasty_students')
  }
}

module.exports = EclesiastyStudentSchema
