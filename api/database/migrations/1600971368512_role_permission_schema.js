'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolePermissionSchema extends Schema {
  up () {
    this.create('role_permissions', (table) => {
      table.bigIncrements('id')
      table.bigInteger('permission_id').index()
      table.foreign('permission_id').references('id').on('permissions').onDelete('cascade')
      table.bigInteger('role_id').index()
      table.foreign('role_id').references('id').on('roles').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('role_permissions')
  }
}

module.exports = RolePermissionSchema
