'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SolicitudSchema extends Schema {
  up () {
    this.create('solicituds', (table) => {
      table.bigIncrements('id')
      table.string('name', 200).notNullable()
      table.string('fullname', 200).notNullable()
      table.string('dni', 13).notNullable()
      table.date('date', 120).notNullable()
      table.string('sexo', 2).notNullable()
      table.string('pais', 20).notNullable()
      table.string('carrera', 20).notNullable()
      table.string('email', 200).notNullable()
      table.boolean('aprovated', 2).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('solicituds')
  }
}

module.exports = SolicitudSchema
