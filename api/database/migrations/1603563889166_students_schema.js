'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentsSchema extends Schema {
  up () {
    this.create('students', (table) => {
      table.bigIncrements('id')
      table.string('name', 200).notNullable()
      table.string('fullname', 200).notNullable()
      table.string('dni', 13).notNullable()
      table.date('date', 120).notNullable()
      table.integer('year')
      table.string('sexo', 2).notNullable()
      table.string('civil_state', 120)
      table.string('pais', 20).notNullable()
      table.string('carrera', 20).notNullable()
      table.string('carrer', 20).notNullable()
      table.string('email', 200).notNullable()
      table.string('phone', 20)
      table.string('local_phone', 20)
      table.string('father_fullname', 200)
      table.string('mother_fullname', 200)
      table.string('father_phone', 20)
      table.string('mother_phone', 20)
      table.string('direction', 200)
      table.string('direction_pais', 30)
      table.string('direction_state', 40)
      table.string('direction_city', 40)
      table.timestamps()
    })
  }

  down () {
    this.drop('students')
  }
}

module.exports = StudentsSchema
