'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AcademicStudentSchema extends Schema {
  up () {
    this.create('academic_students', (table) => {
      table.bigIncrements('id')
      table.bigInteger('user_id').index()
      table.foreign('user_id').references('id').on('students').onDelete('cascade')
      table.string('name', 200).notNullable()
      table.string('dependency', 20).notNullable()
      table.string('especiality', 200).notNullable()
      table.date('year').notNullable()
      table.string('ubicate', 200).notNullable()
      table.string('system', 200).notNullable()
      table.string('superiority_carrera', 200)
      table.string('superiority_insitute', 200)
      table.timestamps()
    })
  }

  down () {
    this.drop('academic_students')
  }
}

module.exports = AcademicStudentSchema
