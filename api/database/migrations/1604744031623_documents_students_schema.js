'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DocumentsStudentsSchema extends Schema {
  up () {
    this.create('documents_students', (table) => {
      table.bigIncrements('id')
      table.bigInteger('user_id').index()
      table.foreign('user_id').references('id').on('students').onDelete('cascade')
      table.string('file', 200)
      table.string('dir', 200)
      table.timestamps()
    })
  }

  down () {
    this.drop('documents_students')
  }
}

module.exports = DocumentsStudentsSchema
